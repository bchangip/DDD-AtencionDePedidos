from flask import Flask
import psutils

app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello World!" + str(psutils.virtual_memory())