import paramiko
k = paramiko.RSAKey.from_private_key_file("ddd-test.pem")
c = paramiko.SSHClient()
c.set_missing_host_key_policy(paramiko.AutoAddPolicy())
print("connecting")
c.connect( hostname = "18.234.99.74", username = "ubuntu", pkey = k )
print("connected")
commands = [ "cd DDD-AtencionDePedidos && git pull" ]
for command in commands:
	print("Executing {}".format(command))
	stdin , stdout, stderr = c.exec_command(command)
	print(stdout.read())
	print("Errors")
	print(stderr.read())
c.close()