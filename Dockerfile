FROM python:3
ADD receiveSubscribe.py /
ADD deploy.py /
ADD ddd-test.pem /
ADD /features /features
ADD requirements.txt /
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
CMD ["python", "./receiveSubscribe.py"]
